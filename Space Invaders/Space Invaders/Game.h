#ifndef GAME_H
#define GAME_H

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/detail/type_vec2.hpp>
#include <vector>
#include "GameObject.h"
#include "TextRenderer.h"


// Represents the current state of the game
enum GameState {
	GAME_ACTIVE,
	GAME_MENU,
	GAME_OVER
};

// Initial size of the player paddle
const glm::vec2 PLAYER_SIZE(150, 100);

const glm::vec2 BULLET_SIZE(100, 20);

const GLfloat PLAYER_VELOCITY(500.0f);

// Game holds all game-related state and functionality.
// Combines all game-related data into a single class for
// easy access to each of the components and manageability.
class Game
{
private:
	std::vector<GameObject *> *bulletArray;
	std::vector<GameObject *> *enemyArray;
	GLuint lives;
	GLuint score;
	TextRenderer  *Text;

	void addEnemy();
	bool checkForCollision(GameObject* a, GameObject* b);
public:
	// Game state
	GameState State;
	GLboolean Keys[1024];
	GLboolean Button[1024];
	GLuint Width, Height;
	// Constructor/Destructor
	Game(GLuint width, GLuint height);
	~Game();
	// Initialize game state (load all shaders/textures/levels)
	void Init();
	// GameLoop
	void ProcessInput(GLfloat dt);
	void Update(GLfloat dt);
	void Render();

	void mouseButtonPressed();
};

#endif