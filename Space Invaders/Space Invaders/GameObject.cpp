#include "GameObject.h"

void GameObject::setRotationVelocity(GLfloat rotationVelocity)
{
	_rotationVelocity = rotationVelocity;
}

GLfloat GameObject::getRotationVelocity()
{
	return _rotationVelocity;
}

void GameObject::setRotation(GLfloat rotation)
{
	this->Rotation = rotation;
}

GLfloat GameObject::getRotation()
{
	return this->Rotation;
}

void GameObject::setPosition(glm::vec2 newPosition)
{
	this->Position = newPosition;
}

glm::vec2 GameObject::getPosition()
{
	return this->Position;
}

void GameObject::setVelocity(glm::vec2 newVelocity)
{
	this->Velocity = newVelocity;
}

glm::vec2 GameObject::getVelocity()
{
	return this->Velocity;
}

GameObject::GameObject()
	: Position(0, 0), Size(1, 1), Velocity(0.0f), Color(1.0f), Rotation(0.0f), _rotationVelocity(0.0f), Sprite(), IsSolid(false), Destroyed(false) { }

GameObject::GameObject(glm::vec2 pos, glm::vec2 size, Texture2D sprite, glm::vec3 color, glm::vec2 velocity)
	: Position(pos), Size(size), Velocity(velocity), Color(color), Rotation(0.0f), _rotationVelocity(0.0f), Sprite(sprite), IsSolid(false), Destroyed(false) { }

void GameObject::Draw(SpriteRenderer &renderer)
{
	renderer.DrawSprite(this->Sprite, this->Position, this->Size, this->Rotation, this->Color);
}

void GameObject::update()
{
	Position += Velocity;
	Rotation += _rotationVelocity;
	//std::cout << _rotation << std::endl;
}