#include "Game.h"
#include "ResourceManager.h"
#include "SpriteRenderer.h"
#include "GameObject.h"

#include <iostream>
#include <sstream>
#include <windows.h>
#include <mmsystem.h>

#include <irrKlang.h>

using namespace irrklang;

ISoundEngine *SoundEngine = createIrrKlangDevice();

#define Square_Size 100

// Game-related State data
SpriteRenderer *renderer;
GameObject      *player;


Game::Game(GLuint width, GLuint height)
	: State(GAME_ACTIVE), Keys(), Button(), Width(width), Height(height), lives(3), score(0)
{

}

Game::~Game()
{
	delete renderer;
	for (std::vector<GameObject *>::iterator spriteIterator = bulletArray->begin(); spriteIterator != bulletArray->end(); spriteIterator++) {
		delete (*spriteIterator);
	}
	delete bulletArray;
	delete player;
}

void Game::Init()
{
	// Load shaders
	ResourceManager::LoadShader("shaders/sprite.vs", "shaders/sprite.frag", nullptr, "sprite");
	// Configure shaders
	glm::mat4 projection = glm::ortho(0.0f, static_cast<GLfloat>(this->Width), static_cast<GLfloat>(this->Height), 0.0f, -1.0f, 1.0f);
	ResourceManager::GetShader("sprite").Use().SetInteger("image", 0);
	ResourceManager::GetShader("sprite").SetMatrix4("projection", projection);
	// Load textures
	ResourceManager::LoadTexture("textures/background.jpg", GL_FALSE, "background");
	ResourceManager::LoadTexture("textures/enemy.png", GL_TRUE, "enemy");
	ResourceManager::LoadTexture("textures/bullet.png", GL_FALSE, "block");
	ResourceManager::LoadTexture("textures/block_solid.png", GL_FALSE, "block_solid");
	ResourceManager::LoadTexture("textures/plane.png", true, "rocket");

	// Load Text
	Text = new TextRenderer(this->Width, this->Height);
	Text->Load("fonts/arial.ttf", 24);

	// Set render-specific controls
	Shader myShader;
	myShader = ResourceManager::GetShader("sprite");
	renderer = new SpriteRenderer(myShader);

	glm::vec2 playerPos = glm::vec2(this->Width / 16 - PLAYER_SIZE.x / 2, this->Height / 2 - PLAYER_SIZE.y);
	player = new GameObject(playerPos, PLAYER_SIZE, ResourceManager::GetTexture("rocket"));

	enemyArray = new std::vector<GameObject*>;
	enemyArray->reserve(20);
	bulletArray = new std::vector<GameObject*>;
	bulletArray->reserve(20);

	SoundEngine->play2D("music/menuMusic.wav", GL_TRUE, GL_FALSE, GL_TRUE);
	
}

void Game::addEnemy()
{
	int locationY = rand() % (unsigned int)Height;
	GameObject *newEnemy = new GameObject(glm::vec2(Width + 100 / 2, locationY), PLAYER_SIZE, ResourceManager::GetTexture("enemy"));
	newEnemy->setVelocity(glm::vec2(-2, 0));
	newEnemy->setRotationVelocity(-0.001);
	enemyArray->push_back(newEnemy);
}

bool Game::checkForCollision(GameObject* a, GameObject* b)
{
	return !(a->getPosition().x + Square_Size / 2 <= b->getPosition().x - Square_Size / 2 ||
		a->getPosition().x + Square_Size / 2 >= b->getPosition().x + Square_Size / 2 ||
		a->getPosition().y + Square_Size / 2 <= b->getPosition().y - Square_Size / 2 ||
		a->getPosition().y - Square_Size / 2 >= b->getPosition().y + Square_Size / 2);
}

void Game::Update(GLfloat dt)
{
	if (this->State == GAME_ACTIVE)
	{
		std::vector<std::vector<GameObject *>::iterator> deleteArray;

		for (std::vector<GameObject *>::iterator spriteIteratorRock = enemyArray->begin(); spriteIteratorRock != enemyArray->end(); spriteIteratorRock++) {
			if (checkForCollision(*spriteIteratorRock, player))
			{
				deleteArray.push_back(spriteIteratorRock);
				if (lives < 1)
				{
					lives = 4;
					score = 0;
					this->State = GAME_OVER;
					SoundEngine->stopAllSounds();
					SoundEngine->play2D("music/menuMusic.wav", GL_TRUE, GL_FALSE, GL_TRUE);
				}
				lives--;
			}

			for (std::vector<GameObject *>::iterator spriteIteratorBall = bulletArray->begin(); spriteIteratorBall != bulletArray->end(); spriteIteratorBall++) {
				if (checkForCollision(*spriteIteratorRock, *spriteIteratorBall))
				{
					score++;
					deleteArray.push_back(spriteIteratorRock);
					deleteArray.push_back(spriteIteratorBall);
				}
			}
		}

		for (std::vector<GameObject *>::iterator spriteIterator = bulletArray->begin(); spriteIterator != bulletArray->end(); spriteIterator++) {
			if (((*spriteIterator)->getPosition()).x >Width + Square_Size)
			{
				deleteArray.push_back(spriteIterator);
				if (lives < 1)
				{
					this->State = GAME_OVER;
					SoundEngine->stopAllSounds();
					SoundEngine->play2D("music/menuMusic.wav", GL_TRUE, GL_FALSE, GL_TRUE);
					lives = 4;
					score = 0;
				}
				lives--;
				std::cout << lives << std::endl;
			}
		}

		for (std::vector<std::vector<GameObject *>::iterator>::iterator deleteIterator = deleteArray.begin(); deleteIterator != deleteArray.end(); deleteIterator++) {
			if ((**deleteIterator)->getVelocity().x > 0)
			{
				bulletArray->erase(*deleteIterator);
			}
			else
			{
				enemyArray->erase(*deleteIterator);
			}
		}

		static int updates;
		if (updates >= 60)
		{
			addEnemy();
			updates = 0;
		}
		++updates;

		for (std::vector<GameObject *>::iterator spriteIterator = bulletArray->begin(); spriteIterator != bulletArray->end(); spriteIterator++) {
			(*spriteIterator)->update();
		}

		for (std::vector<GameObject *>::iterator spriteIterator = enemyArray->begin(); spriteIterator != enemyArray->end(); spriteIterator++) {
			(*spriteIterator)->update();
		}
	}

}

void Game::ProcessInput(GLfloat dt)
{
	if (this->State == GAME_ACTIVE)
	{
		GLfloat velocity = PLAYER_VELOCITY * dt;
		// Move playerboard
		if (this->Keys[GLFW_KEY_A] || this->Keys[GLFW_KEY_LEFT])
		{
			if (player->Position.x >= 0)
				player->Position.x -= velocity;
		}
		if (this->Keys[GLFW_KEY_D] || this->Keys[GLFW_KEY_RIGHT])
		{
			if (player->Position.x <= this->Width - player->Size.x)
				player->Position.x += velocity;
		}
		if (this->Keys[GLFW_KEY_W] || this->Keys[GLFW_KEY_UP])
		{
			if (player->Position.y >= 0)
				player->Position.y -= velocity;
		}
		if (this->Keys[GLFW_KEY_S] || this->Keys[GLFW_KEY_DOWN])
		{
			if (player->Position.y <= this->Height - player->Size.y)
				player->Position.y += velocity;
		}
	}

	if (this->State == GAME_MENU)
	{
		if (this->Keys[GLFW_KEY_ENTER])
		{
			this->State = GAME_ACTIVE;
			SoundEngine->stopAllSounds();
			SoundEngine->play2D("music/music.wav", GL_TRUE, GL_FALSE, GL_TRUE);
		}
	}

	if (this->State == GAME_OVER)
	{
		if (this->Keys[GLFW_KEY_ENTER])
		{
			Init();
			this->State = GAME_ACTIVE;
			SoundEngine->stopAllSounds();
			SoundEngine->play2D("music/music.wav", GL_TRUE, GL_FALSE, GL_TRUE);
		}
	}
}

void Game::mouseButtonPressed()
{
	if (this->State == GAME_ACTIVE)
	{
		//GLfloat velocity = PLAYER_VELOCITY * dt;
		// Move playerboard
		//if (this->Button[GLFW_MOUSE_BUTTON_LEFT])
		//{

		SoundEngine->play2D("music/firing.mp3", GL_FALSE);
			GameObject *bullet = new GameObject(glm::vec2(player->getPosition().x + Square_Size / 2, player->getPosition().y), PLAYER_SIZE, ResourceManager::GetTexture("block"));
			bullet->setVelocity(glm::vec2(3, 0));
			//newEnemy->setRotationVelocity(-0.001);
			bulletArray->push_back(bullet);
			//std::cout << ballsArray->size() << std::endl;
		//}
		/*if (this->Keys[GLFW_KEY_D])
		{
			if (player->Position.x <= this->Width - player->Size.x)
				player->Position.x += velocity;
		}*/
	}
}

void Game::Render()
{
	Texture2D myTexture;
	myTexture = ResourceManager::GetTexture("background");
	renderer->DrawSprite(myTexture, glm::vec2(0, 0), glm::vec2(this->Width, this->Height), 0.0f, glm::vec3(0.3f, 1.0f, 0.6f));

	if (this->State == GAME_ACTIVE)
	{
		player->Draw(*renderer);

		std::stringstream ss; ss << this->lives;
		std::stringstream sc; sc << this->score;
		Text->RenderText("Lives:" + ss.str() + "     Score:" + sc.str(), 5.0f, 5.0f, 1.0f);

		for (std::vector<GameObject *>::iterator spriteIterator = bulletArray->begin(); spriteIterator != bulletArray->end(); spriteIterator++) {
			(*spriteIterator)->Draw(*renderer);
		}

		for (std::vector<GameObject *>::iterator spriteIterator = enemyArray->begin(); spriteIterator != enemyArray->end(); spriteIterator++) {
			(*spriteIterator)->Draw(*renderer);
		}
	}

	if (this->State == GAME_MENU)
	{
		std::stringstream ss; ss << "Press Enter to Start the Game or ESC to Exit";
		Text->RenderText(ss.str(), 500.0f, 400.0f, 1.0f);
	}

	if (this->State == GAME_OVER)
	{
		std::stringstream ss; ss << "Press Enter to try Again or ESC to Exit";
		Text->RenderText(ss.str(), 500.0f, 400.0f, 1.0f);
	}
}